package factory;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Default;
import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@RequestScoped
public class ManagerFactory implements java.io.Serializable {
        
	private static final long serialVersionUID = 1L;

	@PersistenceContext(unitName="app")
    private EntityManager em;
        
    @Produces @RequestScoped @Default
    public EntityManager getEntityManager() {
        return em;
    }               
           
}
