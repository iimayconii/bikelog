package factory;

import java.lang.reflect.ParameterizedType;

import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;
import javax.persistence.EntityManager;

import dao.BasicDao;
import entity.BasicEntity;

public class DaoFactory {
	
  @SuppressWarnings({ "unchecked", "rawtypes" })
  @Produces
  public <T extends BasicEntity> BasicDao<T> persistenceDao(InjectionPoint injectionPoint, EntityManager em) {      
    ParameterizedType type = (ParameterizedType) injectionPoint.getType();
    Class<?> classe = (Class<?>) type.getActualTypeArguments()[0];
    return new BasicDao(classe, em);
  } 
    
  
}
