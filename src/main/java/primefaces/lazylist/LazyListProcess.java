package primefaces.lazylist;

public class LazyListProcess {
	
	private String field;
	private LazyListFormatFilter process;

	public LazyListProcess(String field, LazyListFormatFilter process) {		
		this.field = field;
		this.process = process;
	}	

	public String getField() {
		return field;
	}

	public void setField(String field) {
		this.field = field;
	}

	public LazyListFormatFilter getProcess() {
		return process;
	}

	public void setProcess(LazyListFormatFilter process) {
		this.process = process;
	}
	

}
