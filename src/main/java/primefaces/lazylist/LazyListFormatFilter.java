package primefaces.lazylist;

public interface LazyListFormatFilter {
	public String process(String value);
}
