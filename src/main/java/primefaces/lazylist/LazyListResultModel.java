package primefaces.lazylist;

import java.util.List;

import entity.BasicEntity;

public class LazyListResultModel<T extends BasicEntity> {
	
    private List<T> result;
    private Long count;

    public LazyListResultModel(List<T> result, Long count) {
        this.result = result;
        this.count = count;
    }

    public List<T> getResult() {
        return result;
    }

    public void setResult(List<T> result) {
        this.result = result;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

}
