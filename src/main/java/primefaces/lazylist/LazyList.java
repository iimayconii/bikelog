package primefaces.lazylist;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import com.querydsl.core.types.Predicate;

import entity.BasicEntity;
import repository.Repository;


public class LazyList<T extends BasicEntity> extends LazyDataModel<T>{
	
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LogManager.getLogger(LazyList.class);
	
	private final Repository<T> repository;   
    private List<Predicate> predicate = new ArrayList<Predicate>();
    private List<LazyListProcess> filterFormat = new ArrayList<>();
           
    private SortOrder defaultSort;
    private String defaultSortField;
    private LazyListResultModel<T> lazyResult;
    
    public LazyList(Repository<T> repository) {
        this.repository = repository;
    }       

    @Override
    public Long getRowKey(T object) {
        return object.getId();
    }        
        
    @Override
	public T getRowData(String rowKey) {
    	try {
    		Long key = Long.parseLong(rowKey);    		
    		for(T item : lazyResult.getResult()){
    			if(item.getId().equals(key)){
    				return item;
    			}
    		}
    	}catch(NullPointerException ex) {    
    		logger.warn("Dados são nulos", ex);
    	}catch(NumberFormatException ex) {
    		logger.warn("RowKey não é um long", ex);
    	}catch(Exception ex) {
    		logger.warn("Exceção ocorrida ao procurar o registro de id: " + rowKey, ex);
    	}    	
		return null;
	}

	@Override
    public List<T> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {    	    
    	if(defaultSortField != null && StringUtils.isNotBlank(defaultSortField) && sortField == null){
    		sortField = defaultSortField;
    		sortOrder = defaultSort;
    	}    	
    	String sorterName = sortOrder != null ? sortOrder.name() : "asc";
    	if(!filterFormat.isEmpty()){
    		for(LazyListProcess lp : filterFormat){
    			Object value = filters.get(lp.getField());
    			if(value != null){
    				String valorString = String.valueOf(value);
    				valorString = lp.getProcess().process(valorString);
    				filters.put(lp.getField(), valorString);
    			}
    		}
    	}
        lazyResult = this.repository.lazyLoad(predicate, first, pageSize, sortField, sorterName, filters);    	
        this.setRowCount(lazyResult.getCount().intValue());
        return lazyResult.getResult();
    }
	
	public LazyList<T> addFilterFormat(LazyListProcess p){
		filterFormat.add(p);
		return this;
	}
	
	public LazyList<T> addPredicate(Predicate predicate){
		getPredicate().add(predicate);
		return this;
	}
	
	public List<Predicate> getPredicate() {
		return predicate;
	}
	
	public void setDefaultSort(String sortField, SortOrder defaultSort) {
		this.defaultSort = defaultSort;
		this.defaultSortField = sortField;
	}

	public String getDefaultSortField() {
		return defaultSortField;
	}

	public List<LazyListProcess> getFilterFormat() {
		return filterFormat;
	}
       

}
