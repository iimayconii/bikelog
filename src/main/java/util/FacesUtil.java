package util;

import java.util.Iterator;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

public class FacesUtil {
	
	public static void infoMsg(String msg){
		addMenssage(FacesMessage.SEVERITY_INFO, "Mensagem", msg);
	}
	
	public static void infoMsg(String context, String msg){
		addMenssage(FacesMessage.SEVERITY_INFO, context, msg);
	}
	
	public static void errorMsg(String msg){
		addMenssage(FacesMessage.SEVERITY_ERROR, "Error", msg);
	}
	
	public static void errorMsg(String context, String msg){
		addMenssage(FacesMessage.SEVERITY_ERROR, context, msg);
	}
	
	public static void addMenssage(FacesMessage.Severity tipo, String escopo, String msg) {
		FacesContext context = FacesContext.getCurrentInstance();
		context.getExternalContext().getFlash().setKeepMessages(true);
		context.addMessage(null, new FacesMessage(tipo, escopo, msg));
	}

	public static void addMessage(String msg, FacesMessage.Severity tipo) {
		FacesContext context = FacesContext.getCurrentInstance();
		context.getExternalContext().getFlash().setKeepMessages(true);
		addMessage(msg, tipo);
	}	

    public static void addMessage(String id, String mensagem, FacesMessage.Severity severity) {
        addMessage(id, null, mensagem, severity, false);
    }

    public static void addMessage(String id, String sumario, String mensagem, FacesMessage.Severity severity, boolean clear) {
        if (clear) {
            clearAllMessages();
        }
        FacesContext.getCurrentInstance().addMessage( id, new FacesMessage(severity, mensagem, sumario));        
    }

    public static void clearAllMessages() {
        FacesContext context = FacesContext.getCurrentInstance();
        Iterator<FacesMessage> it = context.getMessages();
        while (it.hasNext()) {
            it.next();
            it.remove();
        }
    }   
}
