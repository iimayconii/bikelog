package entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "tb_bike_log")
public class BikeLog extends BasicEntity{
	
	private static final long serialVersionUID = 1L;
	
	@Column(name = "log", nullable = false, length = 500)
	private String log;
	
	@Column(name = "likes", nullable = false)
	private Integer likes = 0;
	
	@Column(name = "author", nullable = true)
	private String author;
	
	@Column(name = "writing_date", nullable = false)
	private Date writingDate;
	
	public BikeLog() {		
	}	

	public BikeLog(String log, Integer likes, String author, Date writingDate) {		
		this.log = log;
		this.likes = likes;
		this.author = author;
		this.writingDate = writingDate;
	}

	public String getLog() {
		return log;
	}

	public void setLog(String log) {
		this.log = log;
	}

	public Integer getLikes() {
		return likes;
	}

	public void setLikes(Integer likes) {
		this.likes = likes;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public Date getWritingDate() {
		return writingDate;
	}

	public void setWritingDate(Date writingDate) {
		this.writingDate = writingDate;
	}
		
}
