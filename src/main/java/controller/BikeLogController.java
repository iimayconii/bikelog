package controller;

import java.io.Serializable;
import java.util.Date;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import entity.BikeLog;
import primefaces.lazylist.LazyList;
import repository.BikeLogRepository;
import util.FacesUtil;

@Named
@ViewScoped
public class BikeLogController implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LogManager.getLogger(BikeLogRepository.class);
	
	@Inject
	private BikeLogRepository repository;
	private BikeLog bikeLog; 
	private LazyList<BikeLog> lazyList;	
	
	@PostConstruct
	public void postConstruct() {
		this.lazyList = new LazyList<>(repository);		 
		this.bikeLog = new BikeLog();
	}
	
	public void writeBikeLog() {
		try {
			bikeLog.setWritingDate(new Date());
			repository.insert(bikeLog);
			FacesUtil.infoMsg("Mensagem", "Que grande honra ter seu registro sobre a bike, o meio de transporte que amamos!");
			this.bikeLog = new BikeLog();
		}catch(Exception ex) {
			logger.warn(ex);
			ex.printStackTrace();
			FacesUtil.errorMsg("Error", ex.getMessage());
		}
	}

	public LazyList<BikeLog> getLazyList() {
		return lazyList;
	}
	
	public String getTitle() {
		return "Titulo do Controller";
	}

	public BikeLog getBikeLog() {		
		return bikeLog;
	}

	public void setBikeLog(BikeLog bikeLog) {
		this.bikeLog = bikeLog;
	}
	
	
}
