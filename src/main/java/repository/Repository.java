/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository;

import java.util.List;
import java.util.Map;

import com.querydsl.core.types.Predicate;

import dao.exception.DaoException;
import entity.BasicEntity;
import primefaces.lazylist.LazyListResultModel;


/**
 * @author Maycon A J Costa
 * @param <T>
 */
public interface Repository<T extends BasicEntity> {                    
    
	@SuppressWarnings("rawtypes")
	public Class getPersitenceClass();	
    
	public void insert(T entity) throws DaoException;

    public void update(T entity) throws DaoException;
    
    public void remove(T entity) throws DaoException;
	
    public T getInstancePorId(Long id) throws DaoException;                                         

    public T searchByField(String field, String value);
    
    public T getSingleton();
    
    public List<T> getAll();
    
    public LazyListResultModel<T> lazyLoad(List<Predicate> predicates, int first, int pageSize,
    		String sortField, String order, Map<String, Object> values);    
           
}
