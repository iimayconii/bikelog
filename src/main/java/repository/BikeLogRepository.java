package repository;

import javax.transaction.Transactional;

import com.querydsl.core.types.dsl.EntityPathBase;

import entity.BikeLog;
import entity.QBikeLog;

@Transactional
public class BikeLogRepository extends BasicRepository<BikeLog>{

	private static final long serialVersionUID = 1L;	

	@Override
	public EntityPathBase<BikeLog> getEntityPath() {
		return QBikeLog.bikeLog;
	}

	
}
