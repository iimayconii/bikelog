/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.dsl.EntityPathBase;

import dao.BasicDao;
import dao.exception.DaoException;
import entity.BasicEntity;
import primefaces.lazylist.LazyListResultModel;

/**
 *
 * @author maycon
 */
public abstract class BasicRepository<T extends BasicEntity> implements Repository<T>, Serializable {
        
	private static final long serialVersionUID = 1L;
	
	@Inject 
	private BasicDao<T> persistenceDao;

    public abstract EntityPathBase<T> getEntityPath();

    public void insert(T entity) throws DaoException {
        try {
            this.persistenceDao.insert(entity);
        } catch (NullPointerException e) {
            throw new DaoException("Campos Obrigatórios não foram preenchidos!", e);
        }
    }

    public void update(T entity) throws DaoException {
        try {
            this.persistenceDao.update(entity);
        } catch (NullPointerException e) {
            throw new DaoException("Campos Obrigatórios não foram preenchidos!", e);
        }
    }
    
    public void remove(T entity) throws DaoException {
    	this.persistenceDao.delete(this.persistenceDao.findById(entity.getId()));
    }

    
    @Override
    public T getSingleton(){
    	return this.persistenceDao.getSingleton(this.getEntityPath());
    }

    public List<T> getAll() {
    	return persistenceDao.findAll();
    }    

    public T searchByField(String field, String value) {
        return this.persistenceDao.queryFromField(this.getEntityPath(), field, value);
    }
    public T searchByField(HashMap<String, Object> listaFiltros) {
        return this.persistenceDao.queryFromField(this.getEntityPath(), listaFiltros);
    }
    
    @Override
    public Class<T> getPersitenceClass() {
        return this.persistenceDao.getPersistentClass();
    }

    @Override
    public T getInstancePorId(Long id) throws DaoException {
        return this.persistenceDao.findById(id);
    }    

    public BasicDao<T> getPersistenceDao() {
        return persistenceDao;
    }

    @Override
	public LazyListResultModel<T> lazyLoad(List<Predicate> predicates, int first, int pageSize, String sortField, String order,
			Map<String, Object> values) {
    	return this.persistenceDao.lazyLoad(predicates, this.getEntityPath(), first, pageSize, sortField, order, values);
	}
}
