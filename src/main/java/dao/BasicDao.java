/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.transaction.Transactional;
import javax.validation.ConstraintViolationException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.EntityPath;
import com.querydsl.core.types.Order;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.dsl.BooleanPath;
import com.querydsl.core.types.dsl.EntityPathBase;
import com.querydsl.core.types.dsl.Expressions;
import com.querydsl.core.types.dsl.SimplePath;
import com.querydsl.core.types.dsl.StringExpression;
import com.querydsl.jpa.JPAQueryBase;
import com.querydsl.jpa.impl.JPAQuery;
import com.querydsl.jpa.impl.JPAUpdateClause;

import dao.exception.DaoException;
import entity.BasicEntity;
import primefaces.lazylist.LazyListResultModel;

/**
 * @author Maycon J Costa
 * @param <T>
 */
public class BasicDao<T extends BasicEntity> implements java.io.Serializable {

	private static final long serialVersionUID = 1L;	
	private static final Logger logger = LogManager.getLogger(BasicDao.class);
	private static final String sqlCast = "cast({0} as text)"; 

	private final EntityManager em;

    protected final Class<T> persistentClass;

    public BasicDao(Class<T> persistentClass, EntityManager em) {
        this.persistentClass = persistentClass;
        this.em = em;
    }

    public Class<T> getPersistentClass() {
        return persistentClass;
    }

    /**
     * Pesquisa por um id
     *
     * @param id
     * @return
     */
    public T findById(Long id) {
        return em.find(this.getPersistentClass(), id);
    }

    /**
     * Pesquisa todos
     *
     * @return
     */
    public List<T> findAll() {
        CriteriaQuery<T> createQuery = em.getCriteriaBuilder().createQuery(persistentClass);
        createQuery.select(createQuery.from(persistentClass));
        return em.createQuery(createQuery).getResultList();
    }
    
    @SuppressWarnings("unchecked")
	public List<T> findAllWithoutRemoved() {
        Criteria crit = executeCriteria();
        crit.add(Restrictions.ne("removed", true));
        return crit.list();
    }

    /**
     * Executa uma criteria do JPA
     *
     * @return
     */
    public Criteria executeCriteria() {
        Session session = (Session) em.getDelegate();
        return session.createCriteria(persistentClass);        
    }

    /**
     * Insere um dominio
     *
     * @param entity
     */
    public void insert(T entity) {
        em.persist(entity);
    }
    
    public void insertObject(Object entity) {
        em.persist(entity);
    }

    /**
     * Atualiza um dominio
     *
     * @param entity
     */
    public void update(T entity) {
    	em.merge(entity);        
    }

    /**
     * Remove um dominio
     *
     * @param entity
     * @throws Exception
     */
    public void delete(T entity) throws DaoException {
        try {
            if (em.contains(entity)) {
                em.remove(entity);
            } else {
                em.remove(em.find(persistentClass, entity.getId()));
            }
        } catch (PersistenceException e) {
            Throwable t = e.getCause();
            while ((t != null) && !(t instanceof ConstraintViolationException)) {
                t = t.getCause();
            }
            if (t instanceof ConstraintViolationException) {
                throw new DaoException("Não é possivel deletar um registro que esteja vinculado.", e);
            }
            throw e;
        }
    }

    /**
     * Executa uma query nativa
     *
     * @param stringQuery
     * @param parameters
     */
    @Transactional
    public void executeRawQuery(String stringQuery, Map<String, Object> parameters) {    	
        Query query = em.createQuery(stringQuery);        
        for (Entry<String, Object> row : parameters.entrySet()) {
            query.setParameter(row.getKey(), row.getValue());
        }
        query.executeUpdate();
    }

    @Transactional
    public void refresh(T instance) {
        this.em.refresh(instance);
    }

    /**
     * Salva um lista
     *
     * @param entities
     */
    public void saveList(List<T> entities) {
        for (T entity : entities) {
            this.insert(entity);
        }
    }

    /**
     * Cria uma instancia de Query JPA
     *
     * @return
     */
    public JPAQuery<T> query() {    	
        return new JPAQuery<>(this.em);        
    }

    @Transactional
    public JPAUpdateClause updateQuery(EntityPath<T> path) {
        return new JPAUpdateClause(em, path);        
    }

    /**
     * Pega um campo a partir de um string
     *
     * @param property
     * @return
     */
    public Field getFieldFrom(String property) {
        String[] properties = property.split("\\.");
        Class<?> clazz = persistentClass;
        Field field = null;
        try {
            for (String prop : properties) {
            	 if (field != null) {
                     clazz = field.getType();
                 }
            	 field = getFielClass(clazz, prop);
            	 if(field == null){
            		 field = getFielSuperClass(clazz, prop);
            	 }                            
            }
        } catch (SecurityException ex) {            
            logger.warn("Não encontrado o metodo ao realizar a consulta de propriedade", ex);
        }
        return field;
    }
    
    public Field getFielClass(Class<?> clazz, String prop){
    	 try{
         	return clazz.getDeclaredField(prop);
         }catch(NoSuchFieldException | SecurityException ex){
         	logger.warn("Não encontrado campo " + prop + "na class: " + clazz.getName(), ex);
         	return null;
         }      	 
    }
    
    public Field getFielSuperClass(Class<?> clazz, String prop){
   	 	try{
   	 		return clazz.getSuperclass().getDeclaredField(prop);
        }catch(NoSuchFieldException ex){
        	logger.warn("Não encontrado campo " + prop + "na super class: " + clazz.getSuperclass().getName(), ex);
        	return null;
        }     	 	
   }
    
    public Object[] getValueFrom(String property, T instance) {
        String[] properties = property.split("\\.");
        Class<?> clazz = persistentClass;
        Field field = null;               
        Object valueInner = instance;
        try {
            for (String prop : properties) {
                if (field != null) {
                    clazz = field.getType();
                }
                field = getFielClass(clazz, prop);
	           	 if(field == null){
	           		 field = getFielSuperClass(clazz, prop);
	           	 }   
                if(field != null){
                	field.setAccessible(true);
                	valueInner = field.get(valueInner); 
                }
            }
        } catch (SecurityException | IllegalArgumentException | IllegalAccessException ex) {
        	logger.warn("Não foi possivel recuperar o valor do campo: " + property, ex);
        }
        return new Object[]{valueInner, field != null ? field.getType() : null};
    }
    
    
    public LazyListResultModel<T> lazyLoad(List<Predicate> predicates, EntityPathBase<T> entity, int first, int pageSize, String sortField, String order, Map<String, Object> values) {
        BooleanBuilder where = new BooleanBuilder();
        return lazyLoad(predicates, null, entity, first, pageSize, sortField, order, values, where, false);
    }
     
    @SuppressWarnings({ "rawtypes", "unchecked" })
	public LazyListResultModel<T> lazyLoad(List<Predicate> predicates, Map<String, Object> fkEntities, EntityPathBase<T> entity, int first, int pageSize, String sortField, String order, Map<String, Object> values, BooleanBuilder where, Boolean or) {
    	if(predicates != null){
         	for(Predicate e : predicates){
             	where.and(e);
             }
        }   
    	this.applyFilter(entity, fkEntities, where, false);
        this.applyFilter(entity, values, where, or);      
        //BooleanPath removed = (BooleanPath) Expressions.booleanPath(entity, "removed");
        //where.and(removed.isFalse());
        JPAQueryBase queryCount = (JPAQueryBase) this.query().from(entity).where(where);
        Long count = queryCount.fetchCount();
        JPAQueryBase query = (JPAQueryBase) this.query().from(entity).where(where).offset(first).limit(pageSize);
        if (sortField != null && !sortField.isEmpty()) {
            query.orderBy(new OrderSpecifier(order.equalsIgnoreCase("ASCENDING") ? Order.ASC : Order.DESC, Expressions.stringPath(entity, sortField)));
        }        
        List<T> result = (List<T>) query.fetch();        
        return new LazyListResultModel<>(result, count);
    }
    
    @SuppressWarnings({ "rawtypes"})
    private void applyFilter(EntityPathBase<T> entity, Map<String, Object> attrs, BooleanBuilder where, Boolean or) {    	
        if (attrs != null && !attrs.isEmpty()) {
            for (String filterProperty : attrs.keySet()) {
                Field field = this.getFieldFrom(filterProperty);
                if (field != null) {
                    String filterValue = String.valueOf(attrs.get(filterProperty));
                    SimplePath path = Expressions.path(field.getType(), entity, filterProperty);                    
                    StringExpression template = Expressions.stringTemplate(sqlCast, path);
                    if (or) {
                        where.or(template.containsIgnoreCase(filterValue));
                    } else {
                        where.and(template.containsIgnoreCase(filterValue));
                    }
                }
            }
        }
    }
  
    public T queryFromField(EntityPathBase<T> entity, String fieldProperty, String filterValue) {
        BooleanBuilder where = new BooleanBuilder();
        Field field = this.getFieldFrom(fieldProperty);
        if (field != null) {
            SimplePath<Object> path = Expressions.path(field.getType(), entity, fieldProperty);
            where.and(path.eq(filterValue));
        }
        JPAQuery<T> query = this.query();
        query = (JPAQuery<T>) query.from(entity).where(where);
        return (T) query.fetchFirst();        
    }
    
    @SuppressWarnings("rawtypes")
	public T queryFromField(EntityPathBase<T> entity, Map<String, Object> attrs) {
    	BooleanBuilder where = new BooleanBuilder();
    	
    	if (attrs != null && !attrs.isEmpty()) {
             for (Entry<String, Object> filterProperty : attrs.entrySet()) {
                 Field field = this.getFieldFrom(filterProperty.getKey());
                 if (field != null) {
                     String filterValue = String.valueOf(filterProperty.getValue());
                     SimplePath path = Expressions.path(field.getType(), entity, filterProperty.getKey());
                     StringExpression template = Expressions.stringTemplate(sqlCast, path);
                     where.and(template.containsIgnoreCase(filterValue));
                    
                 }
             }
         }
        JPAQuery<T> query = this.query();
        query = (JPAQuery<T>) query.from(entity).where(where);
        return (T) query.fetchFirst();        
    }
    

    public EntityManager getEntityManager() {
        return this.em;
    }

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public T getSingleton(EntityPathBase<T> entity) {
		BooleanBuilder where = new BooleanBuilder();
		//BooleanPath removed = (BooleanPath) Expressions.booleanPath(entity, "removed");
        //where.and(removed.isFalse());
        JPAQueryBase query = this.query();
        query = (JPAQueryBase) query.from(entity).where(where);
        query.limit(1);
        return (T) query.fetchFirst();  
	}


}
